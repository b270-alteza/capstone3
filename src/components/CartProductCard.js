// import { useState, useEffect } from 'react';
import { Card, Button　} from 'react-bootstrap';
import { Link, useNavigate } from 'react-router-dom';
import Swal from "sweetalert2";

export default function CourseCard({courseProp, onDelete}) {

	console.log(courseProp);

	// Deconstructs the course properties into their own variables
	const {_id, productName, productQuantity, productPrice, subTotal } = courseProp;

    //const navigate = useNavigate();

    function deleteFromCart() {
        fetch(`${process.env.REACT_APP_API_URL}/carts/editDeleteProduct`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                productName: productName
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            if(data){
                onDelete(_id);
            }
            else{
                Swal.fire({
                    title: "Error!",
                    icon: "error",
                    text: `Something went wrong. Please try again later!`
                });
            }
        })
    } 

    return (
        <Card>
            <Card.Body>
                <Card.Title>{productName}</Card.Title>
                <Card.Subtitle>Quantity:</Card.Subtitle>
                <Card.Text>{productQuantity}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>{productPrice}</Card.Text>
                <Card.Subtitle>Sub-Total:</Card.Subtitle>
                <Card.Text>PhP {subTotal}</Card.Text>
                <Button variant="primary" as={Link} to={`/products/${_id}`}>Details</Button>
                <Button variant="danger" onClick={deleteFromCart}>Delete</Button>
            </Card.Body>
        </Card>
    )
}