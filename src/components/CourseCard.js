import { useState, useEffect, useContext } from 'react';
import { Card, Button, Form, Modal, Row, Col } from 'react-bootstrap';
import { Link, Navigate, useNavigate } from 'react-router-dom';
import Swal from "sweetalert2";
import UserContext from "../UserContext";

export default function CourseCard({courseProp}) {

	console.log(courseProp);

    const [quantity, setQuantity] = useState(0);

    const navigate = useNavigate();

    const {user} = useContext(UserContext);

    const [showPass, setShowPass] = useState(false);

    const openPass = () => setShowPass(true); 
    const closePass = () => setShowPass(false); 

	// Deconstructs the course properties into their own variables
	const {_id, productId, productName, description, price, stock } = courseProp;

    useEffect(() => {
        if (quantity > stock) {
            setQuantity(stock)
        } else if (quantity < 0){
            setQuantity(0)
        }
    }, [quantity])

    function addToCart(e) {

        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/carts/addToCart`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                addToCart: [
                        {
                            productName: productName,
                            quantity: quantity
                        }
                    ]
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            if(data){
                Swal.fire({
                    title: "Cart Updated!",
                    icon: "success",
                    text: `${productName} x ${quantity} is now in the cart.`
                });
                setQuantity("0")

                closePass()
                navigate("/courses")
            }
            else{
                Swal.fire({
                    title: "Error!",
                    icon: "error",
                    text: `Something went wrong. Please try again later!`
                });
            }

        })
    } 
    
    return (
        <>
            <Card>
                <Card.Body>
                    <Card.Title>{productName}</Card.Title>
                    <Card.Subtitle>Description:</Card.Subtitle>
                    <Card.Text>{description}</Card.Text>
                    <Card.Subtitle>Price:</Card.Subtitle>
                    <Card.Text>PhP {price}</Card.Text>
                    <Card.Text>Stock: {stock}</Card.Text>
                    <Card.Link onClick={openPass} className="mr-2">
                          Details
                    </Card.Link>
                    <Form.Group className="mb-3" controlId="quantity">
                        <Form.Label>No. of Order</Form.Label>
                        <Form.Control 
                            type="number" 
                            placeholder= "0"
                            required
                            value={quantity}
                            onChange={e => setQuantity(e.target.value)}
                        />
                    </Form.Group>

                    {
                        (user.id !== null)
                        ?
                        <Button className="mr-2" variant="primary" onClick={addToCart}>Add to cart</Button>
                        :
                        <Button className="mr-2" variant="primary"  as={Link} to={`/login`}>Login to buy</Button>
                    }
                </Card.Body>
            </Card>
            {/*Modal for Adding a new course*/}
            <Modal show={showPass} fullscreen={true} onHide={closePass}>
                <Form onSubmit={e => addToCart(e)}>
                    <Row>
                        <Col>
                            <Card>
                                <Card.Body className="text-center">
                                    <Card.Title>{productName}</Card.Title>
                                    <Card.Subtitle>Description:</Card.Subtitle>
                                    <Card.Text>{description}</Card.Text>
                                    <Card.Subtitle>Price:</Card.Subtitle>
                                    <Card.Text>PhP {price}</Card.Text>
                                    <Card.Subtitle>Stock</Card.Subtitle>
                                    <Card.Text>{stock}</Card.Text>
                                    <Form.Group className="mb-3 d-flex justify-content-center" controlId="quantity">
                                        <Form.Label className="mt-2 mr-3">No. of Order: </Form.Label>
                                        <Form.Control
                                            className="ml-3 pl-2 w-25"   
                                            type="number" 
                                            placeholder= "0"
                                            required
                                            value={quantity}
                                            onChange={e => setQuantity(e.target.value)}
                                        />
                                    </Form.Group>
                                    <Button className="mr-2" variant="primary" type="submit" id="submitBtn">Add to cart</Button>
                                    <Button variant="secondary" onClick={closePass}>
                                        Close
                                    </Button>
                                </Card.Body>
                            </Card>
                        </Col>
                    </Row>
                </Form>
            </Modal>
            {/*End of modal for adding course*/}
        </>
    )
}