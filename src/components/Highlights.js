import { Row, Col, Card } from 'react-bootstrap';

export default function Highlights() {
	return (
		<Row className="mt-3 mb-3">
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
				    <Card.Body>
				        <Card.Title>Order online</Card.Title>		        
				        <Card.Text>
				          Welcome to our online random store, where the wild and wacky collide! 
				          From rubber chickens to not yet expired snacks(maybe), 
				          explore our eclectic collection and order with excitement 
				          because you never know what delightful surprise might make its way to your doorstep!
				        </Card.Text>				       
				    </Card.Body>
				</Card>
			</Col>

			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
				    <Card.Body>
				        <Card.Title>Exciting upcoming features</Card.Title>		        
				        <Card.Text>
				          Get ready for an exciting upgrade at our garage store! Coming soon, 
				          our premium membership service will unlock a world of mystery boxes, exclusive sales, 
				          and mind-blowing discounts, ensuring that every order is a delightful surprise for the adventurous 
				          souls seeking laughter and savings!
				        </Card.Text>				       
				    </Card.Body>
				</Card>
			</Col>

			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
				    <Card.Body>
				        <Card.Title>About</Card.Title>		        
				        <Card.Text>
				          <div className="contact-info">
				              <p>Contact us:</p>
				              <p>Phone: +1 123-456-7890</p>
				              <p>Fax: +1 987-654-3210</p>
				              <p>Address: 123 Quirky Lane, Peculiarity City, Wonderland</p>
				          </div>
				        </Card.Text>				       
				    </Card.Body>
				</Card>
			</Col>
		</Row>
	)
}