import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, Link, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function CourseView() {

	const navigate = useNavigate();

	// To be able to obtain user ID so we can enroll a user
	const { user } = useContext(UserContext);

	// The "useParams" hook allows us to retrieve the courseId passed via the URL
	const { courseId } = useParams();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [stock, setStock] = useState(0);

	const addToCart = (courseId) => {

		fetch(`${process.env.REACT_APP_API_URL}/users/enroll`, {
			method: "POST",
			headers: {
				"Content-Type" : "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				courseId: courseId
			})
		})
		.then(res=> res.json())
		.then(data => {
			console.log(data);

			if(data) {
				Swal.fire({
					title: "Successfully enrolled!",
					icon: "success",
					text: "You have successfully enrolled for this course."
				})

				navigate("/courses");

			} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				})
			}
		})
	}

	useEffect(() => {

		console.log(courseId)
		fetch(`${process.env.REACT_APP_API_URL}/products/${courseId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setName(data.productName);
			setDescription(data.description);
			setPrice(data.price);
			setStock(data.stock)
		})

	}, [courseId])

	return(
		<Container>
			<Row>
				<Col>
					<Card>
					    <Card.Body className="text-center">
					        <Card.Title>{name}</Card.Title>
					        <Card.Subtitle>Description:</Card.Subtitle>
					        <Card.Text>{description}</Card.Text>
					        <Card.Subtitle>Price:</Card.Subtitle>
					        <Card.Text>PhP {price}</Card.Text>
					        <Card.Subtitle>Stock</Card.Subtitle>
					        <Card.Text>{stock}</Card.Text>
					    </Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	)
}
