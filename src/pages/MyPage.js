import { Fragment, useEffect, useState, useContext } from 'react';
// import coursesData from '../data/coursesData';
//import CartProductCard from '../components/CartProductCard';
import UserContext from '../UserContext';
import { Navigate, useNavigate, Link } from 'react-router-dom';
import { Card, Button, Modal, Form } from 'react-bootstrap';
import Swal from "sweetalert2";

export default function MyPage() {

	const {user} = useContext(UserContext);
	const navigate = useNavigate();

	// State that will be used to store the courses retrieved from the database
	//const [products, setProducts] = useState([]);
	const [newPass1, setNewPass1] = useState("");
	const [newPass2, setNewPass2] = useState("");
	const [oldPass, setOldPass] = useState("");
	const [showPass, setShowPass] = useState(false);

    const [isActive, setIsActive] = useState(false);

	const openPass = () => setShowPass(true); 
	const closePass = () => setShowPass(false); 

	useEffect(() => {

        // Validation to enable submit button when all fields are populated and set a price and slot greater than zero.
        if((oldPass != "" && newPass1 != "" && newPass2 != "" ) && (newPass1 == newPass2)){
            setIsActive(true);
        } else {
            setIsActive(false);
        }

    }, [oldPass, newPass1, newPass2]);

	async function changePass(e) {

		e.preventDefault();

		let res = await fetch(`${process.env.REACT_APP_API_URL}/users/changePassword`, {
			method: "PATCH",
	    	headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				oldPassword: oldPass,
				newPassword: newPass1
			})
	    })
	    const data = await res.json()
        console.log(data);

        if(data){
			Swal.fire({
				title: "Password Change Successful",
				icon: "success",
				text: `Please Re-login.`
			});
			
	        closePass();
	        navigate("/logout")
		} else {
			Swal.fire({
				title: "Wrong Password Unsuccessful",
				icon: "error",
				text: "Something went wrong. Please try again later!"
			});
		}

		setOldPass("");
		setNewPass1("");
		setNewPass2("");
	}

//as={Link} to="/login"
//onClick={openPass}
	return (
		<Fragment>
			<h1>My Profile</h1>
			<div>
				<h3>Email: {user.email}</h3> 
				<br />
				<Button className="mx-2"  onClick={openPass}>
				Change Password
				</Button>
				<Button as={Link} to="/myOrderHistory" variant="primary">
				Order History
				</Button>
			</div>

			{/*Modal for Adding a new course*/}
	        <Modal show={showPass} fullscreen={true} onHide={closePass}>
	    		<Form onSubmit={e => changePass(e)}>

	    			<Modal.Header closeButton>
	    				<Modal.Title>Change Password</Modal.Title>
	    			</Modal.Header>

	    			<Modal.Body>
	    	        	<Form.Group controlId="oldPass" className="mb-3">
	    	                <Form.Label>Old Password</Form.Label>
	    	                <Form.Control 
	    		                type="password" 
	    		                placeholder="Enter Old Password" 
	    		                value = {oldPass}
	    		                onChange={e => setOldPass(e.target.value)}
	    		                required
	    	                />
	    	            </Form.Group>

	    	            <Form.Group controlId="newPass1" className="mb-3">
	    	                <Form.Label>New Password</Form.Label>
	    	                <Form.Control 
	    		                type="password" 
	    		                placeholder="Enter New Password" 
	    		                value = {newPass1}
	    		                onChange={e => setNewPass1(e.target.value)}
	    		                required
	    	                />
	    	            </Form.Group>

	    	            <Form.Group controlId="newPass2" className="mb-3">
	    	                <Form.Label>Re-enter New Password</Form.Label>
	    	                <Form.Control 
	    		                type="password" 
	    		                placeholder="Re-enter New Password" 
	    		                value = {newPass2}
	    		                onChange={e => setNewPass2(e.target.value)}
	    		                required
	    	                />
	    	            </Form.Group>
	    			</Modal.Body>

	    			<Modal.Footer>
	    				{ isActive 
	    					? 
	    					<Button variant="primary" type="submit" id="submitBtn">
	    						Change Password
	    					</Button>
	    				    : 
	    				    <Button variant="danger" type="submit" id="submitBtn" disabled>
	    				    	Change Password
	    				    </Button>
	    				}
	    				<Button variant="secondary" onClick={closePass}>
	    					Close
	    				</Button>
	    			</Modal.Footer>

	    		</Form>	
	    	</Modal>
	    	{/*End of modal for adding course*/}
		</Fragment>


	)
}