import { Fragment, useEffect, useState } from 'react';
// import coursesData from '../data/coursesData';
import CourseCard from '../components/CourseCard';
import UserContext from "../UserContext";
import { useContext } from 'react';

export default function Courses() {
	const [courses, setCourses] = useState([]);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products`)
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setCourses(data.map(course => {
				return(
					<CourseCard key={course._id} courseProp={course}/>
				)
			}))
		})

	}, [])


	return (
		<Fragment>
			<h1>Products</h1>
			{courses}
		</Fragment>
	)
}