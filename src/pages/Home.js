import { Fragment } from 'react';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';


export default function Home() {

	const data = {
	    title: "Bizarre Bazaar",
	    content: "Welcome! Step Inside and Smile at the Unexpected!",
	    destination: "/courses",
	    label: "Browse products"
	}
	
	return (
		<Fragment>
			<Banner data={data}/>
			<Highlights />
		</Fragment>
	)
}