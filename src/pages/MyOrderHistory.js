import { Fragment, useEffect, useState, useContext } from 'react';
// import coursesData from '../data/coursesData';
//import CartProductCard from '../components/CartProductCard';
import UserContext from '../UserContext';
import { Navigate, useNavigate, Link } from 'react-router-dom';
import { Card, Button } from 'react-bootstrap';
import Swal from "sweetalert2";

export default function MyOrderHistory() {
	// Checks to see if the mock data was captured
	// console.log(coursesData);

	const {user} = useContext(UserContext);
	const navigate = useNavigate();

	// State that will be used to store the courses retrieved from the database
	//const [products, setProducts] = useState([]);
	const [orders, setOrders] = useState([]);
	const [totalAmount, setTotalAmount] = useState(0);

	useEffect(()=>{
		fetchData();
	}, [])

	async function fetchData(){
		// get all the courses from the database

		let ordersUri

		if (user.isAdmin) {
			ordersUri = `getAllOrders`
		} else {
			ordersUri = `getOrderHistory`
		}
		let getOrders = await fetch(`${process.env.REACT_APP_API_URL}/orders/${ordersUri}`,{
			method: "GET",
	    	headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			}
		})

		//console.log(getOrders);
		let orders = []
		orders = await getOrders.json()
		// .then(res => res.json())
		// .then(orders => {
		console.log(orders);

			let orderCount = 0;

			if (orders){
				setOrders(orders.map(order => {
					orderCount++;

					return (
						<Card key={order._id}>
						    <Card.Body>
						        <Card.Title>Order #{orderCount}</Card.Title>
						        {
						        	user.isAdmin ? <span>Customer: {order.userId}</span> : null
						        }
		                        <Card.Text>Date Ordered: {(order.dateOrdered).substring(0,10)}</Card.Text>
		                        <Card.Subtitle>Items:</Card.Subtitle>
			                     {
			                     	order.products.map(product => {
			                     							return (
			                     					                	<span>
			                     					                	  {product.productName} x {product.productQuantity} --- PhP {product.productQuantity * product.productPrice}
			                     					                	  <br />
			                     					                	</span>
			                     							)
			                     						})
			                     }
			                     <br />
		                        <Card.Subtitle>Total</Card.Subtitle>
		                        <Card.Text>PhP {order.totalAmount}</Card.Text>
						    </Card.Body>
						</Card>
					)
				}));
			}
			
		// });
	}

	function checkOut() {
		fetch(`${process.env.REACT_APP_API_URL}/orders/buy`,{
			method: "GET",
	    	headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
				    console.log(data);
	            	fetchData();
			})
	}

	function deleteFromCart(productId, productName) {
	    fetch(`${process.env.REACT_APP_API_URL}/carts/editDeleteProduct`, {
	        method: "POST",
	        headers: {
	            'Content-Type': 'application/json',
	            "Authorization": `Bearer ${localStorage.getItem('token')}`
	        },
	        body: JSON.stringify({
	            productName: productName
	        })
	    })
	    .then(res => res.json())
	    .then(data => {
	        console.log(data);

	        if(data){
	            fetchData();
	        }
	        else{
	            Swal.fire({
	                title: "Error!",
	                icon: "error",
	                text: `Something went wrong. Please try again later!`
	            });
	        }
	    })
	} 

	return (
		<Fragment>
			<h1>Order History</h1>
			{orders}
		</Fragment>
	)
}