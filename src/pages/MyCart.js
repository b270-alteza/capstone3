import { Fragment, useEffect, useState, useContext } from 'react';
// import coursesData from '../data/coursesData';
//import CartProductCard from '../components/CartProductCard';
import UserContext from '../UserContext';
import { Navigate, useNavigate, Link } from 'react-router-dom';
import { Card, Button } from 'react-bootstrap';
import Swal from "sweetalert2";

export default function MyCart() {
	// Checks to see if the mock data was captured
	// console.log(coursesData);

	const {user} = useContext(UserContext);
	const navigate = useNavigate();

	// State that will be used to store the courses retrieved from the database
	const [products, setProducts] = useState([]);
	const [totalAmount, setTotalAmount] = useState(0);

	useEffect(()=>{
		fetchData();
	}, [])

	const fetchData = () =>{
		// get all the courses from the database
		fetch(`${process.env.REACT_APP_API_URL}/carts/getAllInMyCart`,{
			method: "GET",
	    	headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(cart => {
			console.log(cart);

			setTotalAmount(cart.totalAmount)

			setProducts(cart.products.map(product => {
				return (
					<Card key={product._id}>
					    <Card.Body>
					        <Card.Title>{product.productName}</Card.Title>
		                        <Card.Subtitle>Quantity:</Card.Subtitle>
		                        <Card.Text>{product.productQuantity}</Card.Text>
		                        <Card.Subtitle>Price:</Card.Subtitle>
		                        <Card.Text>{product.productPrice}</Card.Text>
		                        <Card.Subtitle>Sub-Total:</Card.Subtitle>
		                        <Card.Text>PhP {product.subTotal}</Card.Text>
		                        <Button variant="primary" as={Link} to={`/products/${product._id}`}>Details</Button>
		                        <Button variant="danger" onClick={() => deleteFromCart(product._id, product.productName)}>Delete</Button>
					    </Card.Body>
					</Card>
				)
			}));
		});
	}

	function checkOut() {
		fetch(`${process.env.REACT_APP_API_URL}/orders/buy`,{
			method: "GET",
	    	headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
				    console.log(data);
	            	//fetchData();
				    navigate("/myOrderHistory")

			})
	}

	function deleteFromCart(productId, productName) {
	    fetch(`${process.env.REACT_APP_API_URL}/carts/editDeleteProduct`, {
	        method: "POST",
	        headers: {
	            'Content-Type': 'application/json',
	            "Authorization": `Bearer ${localStorage.getItem('token')}`
	        },
	        body: JSON.stringify({
	            productName: productName
	        })
	    })
	    .then(res => res.json())
	    .then(data => {
	        console.log(data);

	        if(data){
	            fetchData();
	        }
	        else{
	            Swal.fire({
	                title: "Error!",
	                icon: "error",
	                text: `Something went wrong. Please try again later!`
	            });
	        }
	    })
	} 

	return (
		<Fragment>
			<h1>My Cart</h1>
			{products}
			<h2>Total: </h2>
			<h3>PhP {totalAmount}</h3>
            <Button className="mr-2" variant="primary" onClick={checkOut}>Check-out</Button>
		</Fragment>
	)
}